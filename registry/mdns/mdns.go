// Package mdns provides a multicast DNS registry. Implementation https://godoc.org/gitee.com/JMArch/micro/registry/mdns
package mdns

import (
	"gitee.com/JMArch/micro/registry"
	"gitee.com/JMArch/micro/registry/mdns"
)

func NewRegistry(opts ...registry.Option) registry.Registry {
	return mdns.NewRegistry(opts...)
}
