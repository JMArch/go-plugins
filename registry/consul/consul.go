// Package consul provides a Consul registry. Implementation https://godoc.org/gitee.com/JMArch/micro/registry/consul
package consul

import (
	"gitee.com/JMArch/micro/registry"
	"gitee.com/JMArch/micro/registry/consul"
)

func NewRegistry(opts ...registry.Option) registry.Registry {
	return consul.NewRegistry(opts...)
}
