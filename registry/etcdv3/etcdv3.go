// Package etcdv3 provides an etcd version 3 registry
package etcdv3

import (
	"context"
	"crypto/tls"
	"encoding/json"
	"errors"
	"fmt"
	"net"
	"strconv"
	"strings"
	"sync"
	"time"

	"gitee.com/JMArch/micro/cmd"
	"gitee.com/JMArch/micro/registry"
	"go.etcd.io/etcd/clientv3"

	hash "github.com/mitchellh/hashstructure"
	"go.etcd.io/etcd/etcdserver/api/v3rpc/rpctypes"
)

var (
	prefix = "lark.address."
)

type etcdv3Registry struct {
	client  *clientv3.Client
	options registry.Options
	sync.Mutex
	register map[string]uint64
	leases   map[string]clientv3.LeaseID
}

func init() {
	cmd.DefaultRegistries["etcdv3"] = NewRegistry
}

func configure(e *etcdv3Registry, opts ...registry.Option) error {
	config := clientv3.Config{
		Endpoints: []string{"127.0.0.1:2379"},
	}

	for _, o := range opts {
		o(&e.options)
	}

	if e.options.Timeout == 0 {
		e.options.Timeout = 5 * time.Second
	}

	if e.options.Secure || e.options.TLSConfig != nil {
		tlsConfig := e.options.TLSConfig
		if tlsConfig == nil {
			tlsConfig = &tls.Config{
				InsecureSkipVerify: true,
			}
		}

		config.TLS = tlsConfig
	}

	var cAddrs []string

	for _, addr := range e.options.Addrs {
		if len(addr) == 0 {
			continue
		}
		cAddrs = append(cAddrs, addr)
	}

	// if we got addrs then we'll update
	if len(cAddrs) > 0 {
		config.Endpoints = cAddrs
	}

	cli, err := clientv3.New(config)
	if err != nil {
		return err
	}
	e.client = cli
	return nil
}

func encode(s *registry.Service) string {
	b, _ := json.Marshal(s)
	return string(b)
}

func decode(ds []byte) *registry.Service {
	var s *registry.Service
	json.Unmarshal(ds, &s)
	return s
}

func nodePath(s *registry.Service, node *registry.Node) string {
	return fmt.Sprintf("%s%s.%d.%s:%d",
		prefix,
		strings.Replace(s.Name, ".", "/", -1),
		s.MaxProcess,
		node.Address,
		node.Port)
}

func unNodePath(key string) (*registry.Service, error) {
	if !strings.HasPrefix(key, prefix) {
		return nil, fmt.Errorf("unNodePath error: wrong prefix with %s", prefix)
	}

	subs := strings.SplitN(key[len(prefix):], ".", 3)
	// 0 name
	// 1 maxprocess
	// 2 address:port
	if len(subs) != 3 {
		return nil, fmt.Errorf("unNodePath error: wrong format \"%s\"", key[len(prefix):])
	}
	maxProcess, e := strconv.ParseUint(subs[1], 10, 64)
	if e != nil {
		return nil, fmt.Errorf("unNodePath error: maxprocess %s", e.Error())
	}
	address, portStr, e := net.SplitHostPort(subs[2])
	if e != nil {
		return nil, fmt.Errorf("unNodePath error: SplitHostPort %s", e.Error())
	}
	port, _ := strconv.Atoi(portStr)
	return &registry.Service{
		Name:       strings.Replace(subs[0], "/", ".", -1),
		MaxProcess: maxProcess,
		Nodes: []*registry.Node{
			&registry.Node{
				Id:      fmt.Sprintf("<%s>.<%d>", address, port),
				Port:    port,
				Address: address,
			},
		},
	}, nil
}

func servicePath(s string) string {
	return fmt.Sprintf("%s%s", prefix, strings.Replace(s, ".", "/", -1))
}

func (e *etcdv3Registry) Init(opts ...registry.Option) error {
	return configure(e, opts...)
}

func (e *etcdv3Registry) Options() registry.Options {
	return e.options
}

func (e *etcdv3Registry) Deregister(s *registry.Service) error {
	if len(s.Nodes) == 0 {
		return errors.New("Require at least one node")
	}

	e.Lock()
	// delete our hash of the service
	delete(e.register, s.Name)
	// delete our lease of the service
	delete(e.leases, s.Name)
	e.Unlock()

	ctx, cancel := context.WithTimeout(context.Background(), e.options.Timeout)
	defer cancel()

	for _, node := range s.Nodes {
		_, err := e.client.Delete(ctx, nodePath(s, node))
		if err != nil {
			return err
		}
	}
	return nil
}

func (e *etcdv3Registry) Register(s *registry.Service, opts ...registry.RegisterOption) error {
	if len(s.Nodes) == 0 {
		return errors.New("Require at least one node")
	}

	var leaseNotFound bool
	//refreshing lease if existing
	leaseID, ok := e.leases[s.Name]
	if ok {
		if _, err := e.client.KeepAliveOnce(context.TODO(), leaseID); err != nil {
			if err != rpctypes.ErrLeaseNotFound {
				return err
			}

			// lease not found do register
			leaseNotFound = true
		}
	}

	// create hash of service; uint64
	h, err := hash.Hash(s, nil)
	if err != nil {
		return err
	}

	// get existing hash
	e.Lock()
	v, ok := e.register[s.Name]
	e.Unlock()

	// the service is unchanged, skip registering
	if ok && v == h && !leaseNotFound {
		return nil
	}

	service := &registry.Service{
		MaxProcess: s.MaxProcess,
		Name:       s.Name,
		Version:    s.Version,
		Metadata:   s.Metadata,
		Endpoints:  s.Endpoints,
	}

	var options registry.RegisterOptions
	for _, o := range opts {
		o(&options)
	}

	ctx, cancel := context.WithTimeout(context.Background(), e.options.Timeout)
	defer cancel()

	var lgr *clientv3.LeaseGrantResponse
	if options.TTL.Seconds() > 0 {
		lgr, err = e.client.Grant(ctx, int64(options.TTL.Seconds()))
		if err != nil {
			return err
		}
	}

	for _, node := range s.Nodes {
		service.Nodes = []*registry.Node{node}
		if lgr != nil {
			_, err = e.client.Put(ctx, nodePath(service, node), service.Version, clientv3.WithLease(lgr.ID))
		} else {
			_, err = e.client.Put(ctx, nodePath(service, node), service.Version)
		}
		if err != nil {
			return err
		}
	}

	e.Lock()
	// save our hash of the service
	e.register[s.Name] = h
	// save our leaseID of the service
	if lgr != nil {
		e.leases[s.Name] = lgr.ID
	}
	e.Unlock()

	return nil
}

func (e *etcdv3Registry) GetService(name string) ([]*registry.Service, error) {
	ctx, cancel := context.WithTimeout(context.Background(), e.options.Timeout)
	defer cancel()

	rsp, err := e.client.Get(ctx, servicePath(name)+".", clientv3.WithPrefix(), clientv3.WithSort(clientv3.SortByKey, clientv3.SortDescend))
	if err != nil {
		return nil, err
	}

	if len(rsp.Kvs) == 0 {
		return nil, registry.ErrNotFound
	}

	serviceMap := map[string]*registry.Service{}

	for _, n := range rsp.Kvs {
		key := string(n.Key)
		version := string(n.Value)
		tmp, e := unNodePath(key)
		if e != nil {
			continue
		}
		s, ok := serviceMap[version]
		if !ok {
			tmp.Version = version
			serviceMap[tmp.Version] = tmp
			s = tmp
		} else {
			for _, node := range tmp.Nodes {
				s.Nodes = append(s.Nodes, node)
			}
		}
	}

	var services []*registry.Service
	for _, service := range serviceMap {
		services = append(services, service)
	}
	return services, nil
}

func (e *etcdv3Registry) ListServices() ([]*registry.Service, error) {
	var services []*registry.Service
	nameSet := make(map[string]struct{})

	ctx, cancel := context.WithTimeout(context.Background(), e.options.Timeout)
	defer cancel()

	rsp, err := e.client.Get(ctx, prefix, clientv3.WithPrefix(), clientv3.WithSort(clientv3.SortByKey, clientv3.SortDescend))
	if err != nil {
		return nil, err
	}

	if len(rsp.Kvs) == 0 {
		return []*registry.Service{}, nil
	}

	for _, n := range rsp.Kvs {
		if sn := decode(n.Value); sn != nil {
			nameSet[sn.Name] = struct{}{}
		}
	}
	for k := range nameSet {
		service := &registry.Service{}
		service.Name = k
		services = append(services, service)
	}

	return services, nil
}

func (e *etcdv3Registry) Watch(opts ...registry.WatchOption) (registry.Watcher, error) {
	return newEtcdv3Watcher(e, e.options.Timeout, opts...)
}

func (e *etcdv3Registry) String() string {
	return "etcdv3"
}

func NewRegistry(opts ...registry.Option) registry.Registry {
	e := &etcdv3Registry{
		options:  registry.Options{},
		register: make(map[string]uint64),
		leases:   make(map[string]clientv3.LeaseID),
	}
	configure(e, opts...)
	return e
}
