package weights

import "gitee.com/JMArch/micro/registry"

type weightsNode struct {
	node    *registry.Node
	weights uint64
}

type weightsNodes []*weightsNode

func (w weightsNodes) Len() int {
	return len(w)
}

func (w weightsNodes) Swap(i, j int) {
	w[i], w[j] = w[j], w[i]
}

func (w weightsNodes) Less(i, j int) bool {
	return w[i].weights < w[j].weights
}
