package weights

import (
	"context"
	"errors"
	"math/rand"
	"sort"

	"gitee.com/JMArch/micro/cmd"
	"gitee.com/JMArch/micro/registry"
	"gitee.com/JMArch/micro/selector"
)

var (
	ErrNoneAvailable = errors.New("none available")
)

type weightsSelector struct {
	so selector.Options
}

func init() {
	cmd.DefaultSelectors["weights"] = NewSelector
}

func next(nodes []*weightsNode, totalWeights uint64) func() (*registry.Node, error) {
	return func() (*registry.Node, error) {
		if len(nodes) == 0 {
			return nil, ErrNoneAvailable
		}
		i := uint64(rand.Int63n(int64(totalWeights)))
		var sum uint64
		for _, node := range nodes {
			sum += node.weights
			if i < sum {
				return node.node, nil
			}
		}
		return nil, ErrNoneAvailable
	}
}

func (r *weightsSelector) Init(opts ...selector.Option) error {
	for _, o := range opts {
		o(&r.so)
	}
	return nil
}

func (r *weightsSelector) Options() selector.Options {
	return r.so
}

func (r *weightsSelector) Select(service string, opts ...selector.SelectOption) (selector.Next, error) {
	var sopts selector.SelectOptions
	for _, opt := range opts {
		opt(&sopts)
	}

	// get the service
	services, err := r.so.Registry.GetService(service)
	if err != nil {
		return nil, err
	}

	// apply the filters
	for _, filter := range sopts.Filters {
		services = filter(services)
	}

	// if there's nothing left, return
	if len(services) == 0 {
		return nil, selector.ErrNotFound
	}

	var nodes weightsNodes
	var total uint64

	// flatten node list
	for _, service := range services {
		if service.MaxProcess == 0 {
			continue
		}
		for _, node := range service.Nodes {
			nodes = append(nodes, &weightsNode{
				node:    node,
				weights: service.MaxProcess,
			})
			total += service.MaxProcess
		}
	}

	// 按照处理能力，升序排列
	sort.Sort(nodes)

	return next(nodes, total), nil
}

func (r *weightsSelector) Mark(service string, node *registry.Node, err error) {
	return
}

func (r *weightsSelector) Reset(service string) {
	return
}

func (r *weightsSelector) Close() error {
	return nil
}

func (r *weightsSelector) String() string {
	return "weights"
}

func NewSelector(opts ...selector.Option) selector.Selector {
	sopts := selector.Options{
		Context:  context.TODO(),
		Registry: registry.DefaultRegistry,
	}

	for _, opt := range opts {
		opt(&sopts)
	}

	return &weightsSelector{sopts}
}
