// Package tcp provides a TCP transport
package tcp

import (
	"bufio"
	"crypto/tls"
	"errors"
	"fmt"
	"io"
	"net"
	"time"

	"gitee.com/JMArch/micro/cmd"
	"gitee.com/JMArch/micro/transport"
	"github.com/micro/go-log"
	maddr "github.com/micro/util/go/lib/addr"
	mnet "github.com/micro/util/go/lib/net"
	mls "github.com/micro/util/go/lib/tls"
	grace "github.com/sunreaver/grace/gracenet"
)

type tcpTransport struct {
	opts transport.Options
}

type tcpTransportClient struct {
	dialOpts transport.DialOptions
	conn     net.Conn
	enc      *bufio.Writer
	dec      io.Reader
	timeout  time.Duration
}

type tcpTransportSocket struct {
	conn    net.Conn
	enc     *bufio.Writer
	dec     io.Reader
	timeout time.Duration
}

type tcpTransportListener struct {
	listener net.Listener
	timeout  time.Duration
	graceNet *grace.Net
}

func init() {
	cmd.DefaultTransports["tcp"] = NewTransport
}

func (t *tcpTransportClient) Send(m *transport.Message) error {
	// set timeout if its greater than 0
	if t.timeout > time.Duration(0) {
		t.conn.SetDeadline(time.Now().Add(t.timeout))
	}

	if _, err := t.enc.Write(m.Body); err != nil {
		return err
	}
	return t.enc.Flush()
}

func (t *tcpTransportClient) Recv(m *transport.Message) error {
	// set timeout if its greater than 0
	if t.timeout > time.Duration(0) {
		t.conn.SetDeadline(time.Now().Add(t.timeout))
	}
	data, e := ReadAllData(t.dec)
	m.Header = map[string]string{}
	m.Body = data
	return e
}

func (t *tcpTransportClient) Close() error {
	return t.conn.Close()
}

func (t *tcpTransportSocket) Recv(m *transport.Message) error {
	if m == nil {
		return errors.New("message passed in is nil")
	}

	// set timeout if its greater than 0
	if t.timeout > time.Duration(0) {
		t.conn.SetDeadline(time.Now().Add(t.timeout))
	}

	data, e := ReadAllData(t.dec)
	m.Header = map[string]string{}
	m.Body = data
	return e
}

func (t *tcpTransportSocket) Send(m *transport.Message) error {
	// set timeout if its greater than 0
	if t.timeout > time.Duration(0) {
		t.conn.SetDeadline(time.Now().Add(t.timeout))
	}
	_, e := t.enc.Write(m.Body)
	if e != nil {
		return e
	}
	return t.enc.Flush()
}

func (t *tcpTransportSocket) Close() error {
	return t.conn.Close()
}

func (t *tcpTransportListener) GraceRestart() (pid int, err error) {
	return t.graceNet.StartProcess()
}

func (t *tcpTransportListener) Addr() string {
	return t.listener.Addr().String()
}

func (t *tcpTransportListener) Close() error {
	return t.listener.Close()
}

func (t *tcpTransportListener) Accept(fn func(transport.Socket)) error {
	var tempDelay time.Duration

	for {
		c, err := t.listener.Accept()
		if err != nil {
			if ne, ok := err.(net.Error); ok && ne.Temporary() {
				if tempDelay == 0 {
					tempDelay = 5 * time.Millisecond
				} else {
					tempDelay *= 2
				}
				if max := 1 * time.Second; tempDelay > max {
					tempDelay = max
				}
				log.Logf("http: Accept error: %v; retrying in %v\n", err, tempDelay)
				time.Sleep(tempDelay)
				continue
			}
			return err
		}

		sock := &tcpTransportSocket{
			timeout: t.timeout,
			conn:    c,
			enc:     bufio.NewWriter(c),
			dec:     c,
		}

		go func() {
			// TODO: think of a better error response strategy
			defer func() {
				if r := recover(); r != nil {
					sock.Close()
				}
			}()

			fn(sock)
		}()
	}
}

func (t *tcpTransport) Dial(addr string, opts ...transport.DialOption) (transport.Client, error) {
	dopts := transport.DialOptions{
		Timeout: transport.DefaultDialTimeout,
	}

	for _, opt := range opts {
		opt(&dopts)
	}

	var conn net.Conn
	var err error

	// TODO: support dial option here rather than using internal config
	if t.opts.Secure || t.opts.TLSConfig != nil {
		config := t.opts.TLSConfig
		if config == nil {
			config = &tls.Config{
				InsecureSkipVerify: true,
			}
		}
		conn, err = tls.DialWithDialer(&net.Dialer{Timeout: dopts.Timeout}, "tcp", addr, config)
	} else {
		conn, err = net.DialTimeout("tcp", addr, dopts.Timeout)
	}

	if err != nil {
		return nil, err
	}

	return &tcpTransportClient{
		dialOpts: dopts,
		conn:     conn,
		enc:      bufio.NewWriter(conn),
		dec:      conn,
		timeout:  t.opts.Timeout,
	}, nil
}

func (t *tcpTransport) Listen(addr string, opts ...transport.ListenOption) (transport.Listener, error) {
	var options transport.ListenOptions
	for _, o := range opts {
		o(&options)
	}

	graceNet := grace.Net{}

	var l net.Listener
	var err error

	// TODO: support use of listen options
	if t.opts.Secure || t.opts.TLSConfig != nil {
		config := t.opts.TLSConfig

		fn := func(addr string) (net.Listener, error) {
			if config == nil {
				hosts := []string{addr}

				// check if its a valid host:port
				if host, _, err := net.SplitHostPort(addr); err == nil {
					if len(host) == 0 {
						hosts = maddr.IPs()
					} else {
						hosts = []string{host}
					}
				}

				// generate a certificate
				cert, err := mls.Certificate(hosts...)
				if err != nil {
					return nil, err
				}
				config = &tls.Config{Certificates: []tls.Certificate{cert}}
			}
			return tls.Listen("tcp", addr, config)
		}

		l, err = mnet.Listen(addr, fn)
	} else {
		fn := func(addr string) (net.Listener, error) {
			return graceNet.Listen("tcp", addr)
		}

		l, err = mnet.Listen(addr, fn)
	}

	if err != nil {
		return nil, err
	}

	return &tcpTransportListener{
		timeout:  t.opts.Timeout,
		listener: l,
		graceNet: &graceNet,
	}, nil
}

func (t *tcpTransport) Init(opts ...transport.Option) error {
	for _, o := range opts {
		o(&t.opts)
	}
	return nil
}

func (t *tcpTransport) Options() transport.Options {
	return t.opts
}

func (t *tcpTransport) String() string {
	return "tcp"
}

func NewTransport(opts ...transport.Option) transport.Transport {
	var options transport.Options
	for _, o := range opts {
		o(&options)
	}
	return &tcpTransport{opts: options}
}

// ReadAllData 读取大量数据
func ReadAllData(reader io.Reader) ([]byte, error) {
	const onceReadSize = 1024
	const maxReadSize = 1024 * 1024 * 10 // 10MB
	out := make([]byte, onceReadSize)
	seed := 0
	for {
		// n, e := io.ReadFull(reader, out[seed:])
		n, e := reader.Read(out[seed:])
		if e != nil {
			return nil, e
		} else if n < onceReadSize {
			// 读取完毕
			out = out[:seed+n]
			break
		}
		out = out[:seed+n]
		tmp := make([]byte, n) // 保证尾部空闲位置一直为 onceReadSize
		out = append(out, tmp[:]...)
		seed += n
		if seed > maxReadSize {
			return nil, fmt.Errorf("body large than %dbyte", maxReadSize)
		}
	}
	return out, nil
}
