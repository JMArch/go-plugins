// Package http provides a http transport
package http

import (
	"gitee.com/JMArch/micro/transport"
	"gitee.com/JMArch/micro/transport/http"
)

/*
	HTTP transport is the default synchronous communication mechanism for go-micro.
	Implementation here https://godoc.org/gitee.com/JMArch/micro/transport/http
	We add a link here for completeness
*/

func NewTransport(opts ...transport.Option) transport.Transport {
	return http.NewTransport(opts...)
}
